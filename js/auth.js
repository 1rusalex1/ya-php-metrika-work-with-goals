/**
 * Created by admin on 03.02.2016.
 */
$(document).ready(function () {
    var authContainer = opener.$('#auth-container'),
        formContainer = opener.$('#form-container'),
        token = /access_token=([0-9a-f]+)/.exec(document.location.hash),
        expires = /expires_in=([0-9a-f]+)/.exec(document.location.hash);
    $.cookie("ya_token", token[1], {expires: Number(expires[1])});
    opener.APP.showElements(formContainer);
    opener.APP.hideElements(authContainer);
    window.close();
});
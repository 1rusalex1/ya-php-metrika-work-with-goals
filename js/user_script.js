/**
 * Created by admin on 02.02.2016.
 */


APP = (function ($) {
    var authInput = $('#auth'),
        authContainer = $('#auth-container'),
        formContainer = $('#form-container'),
        alertBox = $('#alert'),
        submitButton = $('#set-goals');

    var inputParams;


    function init() {
        addHadlers();
        if (!isYaToken()) {
            showElements(authContainer);
            hideElements(formContainer);
        } else {
            showElements(formContainer);
            hideElements(authContainer);
        }
    }

    function addHadlers() {
        authInput.click(function () {
            var newWin = window.newWindow = window.open("https://oauth.yandex.ru/authorize?response_type=token&client_id=b1f6ed1dbd64492dabbb2a4cbf579888",
                "new_win",
                "resizable=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, width = 608, height = 640, top = 50, left = " + ((window.innerWidth - 585) / 2));
        });
        submitButton.click(function () {
            setInputParams();
            if (!inputParams) {
                showAlert('Заполните все поля.', 'alert-danger');
            } else {
                sendGoals($.cookie('ya_token'), inputParams);
            }


        });
    }

    function isYaToken() {
        return !!$.cookie('ya_token');
    }

    function setInputParams() {
        var obj = {};
        $('input').each(function () {
            var $this = $(this),
                key = $this.attr('data-input'),
                value = $this.val();
            if (!value) {
                obj.error = true;
            } else if (key == 'prefix') {
                obj[key] = value;
            } else {
                obj[key] = obj['prefix'] + '-' + value;
            }
        });

        inputParams = (!obj.error) ? obj : false;
    }

    function showElements(elements) {
        var el;
        for (el in elements) if (elements.hasOwnProperty(el)) {
            $(elements[el]).removeClass('hidden');
        }
    }

    function hideElements(elements) {
        var el;
        for (el in elements) if (elements.hasOwnProperty(el)) {
            $(elements[el]).addClass('hidden');
        }
    }

    function sendGoals(yaToken, params) {
        $.ajax({
            url: "ya_goals.php",
            type: "POST",
            data: {
                yaToken: yaToken,
                params: params
            },
            success: function () {
                showAlert('Цели установлены успешно.', 'alert-success');
            }

        })
    }

    function showAlert(message, type) {
        alertBox.addClass(type).html(message);
        setTimeout(function () {
            alertBox.removeClass(type).html('');
        }, 3000);
    }


    return {
        init: init,
        showElements: showElements,
        hideElements: hideElements
    }

})(jQuery);

APP.init();
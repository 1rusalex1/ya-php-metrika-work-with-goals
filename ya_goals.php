<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.02.2016
 */

require_once 'phar://libs/yandex-php-library_1.0.0.phar/vendor/autoload.php';
require_once 'PhpClasses/ComplexGoal.php';

use Yandex\Metrica\Management\AvailableValues;
use Yandex\Metrica\Management\GoalsClient;

class YaGoals
{

    private $goalClient;
    private $complexGoal;
    private $simpleGoal;


    public function __construct()
    {
        $params = $_POST['params'];
        $this->goalClient = new GoalsClient($_POST['yaToken']);
        $this->setSimpleGoal($params['simple-goal-name'], $params['simple-goal-event']);
        $this->setComplexGoal(
            $params['complex-goal-name'],
            $params['first-step-name'],
            $params['first-step-event'],
            $params['second-step-name'],
            $params['second-step-event'],
            $params['third-step-name'],
            $params['third-step-event']
        );

    }

    /**
     * @param $name
     * @param $event
     * @internal param mixed $simpleGoal
     */
    public function setSimpleGoal($name, $event)
    {
        $conditions = self::createConditions(AvailableValues::GOAL_CONDITIONS_TYPE_EXACT, $event);
        $this->simpleGoal = new \Yandex\Metrica\Management\Models\Goal();
        $this->simpleGoal
            ->setName($name)
            ->setType(AvailableValues::GOAL_TYPE_ACTION)
            ->setConditions($conditions);

        $this->goalClient->addGoal('35128620', $this->simpleGoal);
    }

    /**
     * @param $type
     * @param $event
     * @return \Yandex\Metrica\Management\Models\Conditions
     */
    public static function createConditions($type, $event)
    {
        $condition = new \Yandex\Metrica\Management\Models\Condition();
        $condition
            ->setType($type)
            ->setUrl($event);
        $conditions = new \Yandex\Metrica\Management\Models\Conditions();
        $conditions->add($condition);
        return $conditions;
    }

    /**
     * @param $name
     * @param $nameFirstStep
     * @param $eventFirstStep
     * @param $nameSecondStep
     * @param $eventSecondStep
     * @param $nameThirdStep
     * @param $eventThirdStep
     * @internal param mixed $complexGoal
     */
    public function setComplexGoal($name, $nameFirstStep, $eventFirstStep, $nameSecondStep, $eventSecondStep, $nameThirdStep, $eventThirdStep)
    {
        $steps = new \Yandex\Metrica\Management\Models\Goals();

        $conditions = self::createConditions(AvailableValues::GOAL_CONDITIONS_TYPE_ACTION, $eventFirstStep);
        $step = self::createStep($nameFirstStep, $conditions);
        $steps->add($step);
        $conditions = self::createConditions(AvailableValues::GOAL_CONDITIONS_TYPE_ACTION, $eventSecondStep);
        $step = self::createStep($nameSecondStep, $conditions);
        $steps->add($step);
        $conditions = self::createConditions(AvailableValues::GOAL_CONDITIONS_TYPE_ACTION, $eventThirdStep);
        $step = self::createStep($nameThirdStep, $conditions);
        $steps->add($step);
        $this->complexGoal = new ComplexGoal();
        $this->complexGoal
            ->setName($name)
            ->setType(AvailableValues::GOAL_TYPE_STEP)
            ->setClass(AvailableValues::GOAL_CLASS_0)
            ->setSteps($steps);
        $this->goalClient->addGoal('35128620', $this->complexGoal);

    }

    /**
     * @param $name
     * @param \Yandex\Metrica\Management\Models\Conditions $conditions
     * @return \Yandex\Metrica\Management\Models\Goal
     */
    public static function createStep($name, \Yandex\Metrica\Management\Models\Conditions $conditions)
    {
        $step = new \Yandex\Metrica\Management\Models\Goal();
        $step
            ->setName($name)
            ->setType(AvailableValues::GOAL_TYPE_URL)
            ->setClass(AvailableValues::GOAL_CLASS_0)
            ->setConditions($conditions);
        return $step;
    }


}

new YaGoals();